//
//  RepositoryTests.swift
//  WesterosTests
//
//  Created by Jorge Beltran Nuñez on 13/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import XCTest

@testable import Westeros

class RepositoryTests: XCTestCase {
    var localHouses: [House]!
    var localSeasons: [Season]!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        localHouses = Repository.local.houses
        localSeasons = Repository.local.seasons
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLocalRespositoryCreation() {
        let localRepository = Repository.local
        XCTAssertNotNil(localRepository)
    }
    
    func testLocalRespositoryHousesCreation(){
        XCTAssertNotNil(localHouses)
        XCTAssertGreaterThan(localHouses.count, 0)
    }
    
    func testLocalRespositorySeasonsCreation(){
        XCTAssertNotNil(localSeasons)
        XCTAssertGreaterThan(localSeasons.count, 0)
    }
    
    func testLocalRespositoryReturnsSortedArrayOfHouses(){
        XCTAssertEqual(localHouses, localHouses.sorted())
    }
    
    func testLocalRespositoryReturnsSortedArrayOfSeasons(){
        XCTAssertEqual(localSeasons, localSeasons.sorted())
    }
    
    func testLocalRepositoryReturnsHousesByCaseInsensitively() {
        let stark = Repository.local.house(named: "sTarK")
        XCTAssertEqual(stark?.name, .stark)
        
        let keepcoding = Repository.local.house(named: "Keepcoding")
        XCTAssertNil(keepcoding)
    }
    
    func testLocalRepositoryReturnsSeasonsByCaseInsensitively() {
        let seasonOne = Repository.local.season(titled: "TEmPoRaDa UnO")
        XCTAssertEqual(seasonOne?.title, "Temporada uno")
        
        let seasonTen = Repository.local.season(titled: "Temporada diez")
        XCTAssertNil(seasonTen)
    }
    
    func testHouseFiltering(){
        let countHouses = Repository.local.houses.count
        
        let filtered = Repository.local.houses(filteredBy: {$0.count > 0})
        XCTAssertEqual(filtered.count, countHouses)
        
        let otherFilter = Repository.local.houses(filteredBy: {$0.words.contains("invierno")})
        XCTAssertEqual(otherFilter.count, 1)
    }
    
    func testSeasonFiltering(){
        let countSeason = Repository.local.seasons.count
        
        let filtered = Repository.local.seasons(filteredBy: {$0.count > 0})
        XCTAssertEqual(filtered.count, countSeason)
        
        let otherFilter = Repository.local.seasons(filteredBy: {$0.title.contains("tres")})
        XCTAssertEqual(otherFilter.count, 1)
    }
}
