//
//  SeasonTests.swift
//  WesterosTests
//
//  Created by Jorge Beltran Nuñez on 22/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import XCTest

@testable import Westeros

class SeasonTests: XCTestCase {
    
    var seasonOne: Season!
    var espisodeOneOfOne: Episode!
    var espisodeTwoOfOne: Episode!
    
    var seasonTwo: Season!
    var espisodeOneOfTwo: Episode!
    var espisodeTwoOfTwo: Episode!
    var espisodeThreeOfTwo: Episode!
    
    override func setUp() {
        super.setUp()
        seasonOne = Season(order: 1, title: "Temporada uno", releaseDate: "16-07-1997")
        espisodeOneOfOne = Episode(order: 1, title: "Uno de uno", releaseDate: "16-07-1997", season: seasonOne)
        espisodeTwoOfOne = Episode(order: 2, title: "Dos de uno", releaseDate: "16-07-1997", season: seasonOne)
        
        seasonTwo = Season(order: 2, title: "Temporada dos", releaseDate: "16-07-1997")
        espisodeOneOfTwo = Episode(order: 1, title: "Uno de dos", releaseDate: "16-07-1997", season: seasonTwo)
        espisodeTwoOfTwo = Episode(order: 2, title: "Dos de dos", releaseDate: "16-07-1997", season: seasonTwo)
        espisodeThreeOfTwo = Episode(order: 3, title: "Tres de dos", releaseDate: "16-07-1997", season: seasonTwo)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSeasonExistance() {
        XCTAssertNotNil(seasonOne)
        XCTAssertNotNil(seasonTwo)
    }
    
    func testCustomStringConvertible() {
        XCTAssertNotNil(seasonOne.description)
    }
    
    func testSeasonDescription() {
        let seasonOneOfOneEqual = Season(order: 1, title: "Temporada uno", releaseDate: "16-07-1997")
        XCTAssertEqual(seasonOne.description, seasonOneOfOneEqual.description)
    }
    
    func testSeasonEquality() {
        XCTAssertEqual(seasonOne, seasonOne)
        
        let seasonOneOfOneEqual = Season(order: 1, title: "Temporada uno", releaseDate: "16-07-1997")
        XCTAssertEqual(seasonOne, seasonOneOfOneEqual)
        
        XCTAssertNotEqual(seasonOne, seasonTwo)
        
        let seasonOneOfOneNotEqual = Season(order: 2, title: "Temporada uno", releaseDate: "16-07-1997")
        XCTAssertNotEqual(seasonOne, seasonOneOfOneNotEqual)
    }
    
    func testSeasonHashable() {
        XCTAssertNotNil(seasonOne.hashValue)
    }
    
    func testSeasonComparison(){
        XCTAssertLessThan(seasonOne, seasonTwo)
    }
    
    func testAddEpisodes(){
        XCTAssertEqual(seasonOne.count, 2)
        
        _ = Episode(order: 3, title: "Tres de uno", releaseDate: "16-07-1997", season: seasonOne)

        XCTAssertEqual(seasonOne.count, 3)
    }
    func testHouseReturnsSortedArrayOfEpisodes() {
        seasonTwo.add(episodes: espisodeOneOfTwo, espisodeThreeOfTwo, espisodeTwoOfTwo)

        XCTAssertEqual(seasonTwo.sortedEpisodes, [espisodeOneOfTwo, espisodeTwoOfTwo, espisodeThreeOfTwo])
        XCTAssertNotEqual(seasonTwo.sortedEpisodes, [espisodeOneOfTwo, espisodeThreeOfTwo, espisodeTwoOfTwo])
    }
}
