//
//  EpisodeTests.swift
//  WesterosTests
//
//  Created by Jorge Beltran Nuñez on 22/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import XCTest

@testable import Westeros

class EpisodeTests: XCTestCase {
    
    var espisodeOneOfOne: Episode!
    var espisodeOneOfTwo: Episode!
    var seasonOne: Season!
    
    override func setUp() {
        super.setUp()
        seasonOne = Season(order: 1, title: "Temporada uno", releaseDate: "16-07-1997")
        espisodeOneOfOne = Episode(order: 1, title: "Uno de uno", releaseDate: "16-07-1997", season: seasonOne)
        espisodeOneOfTwo = Episode(order: 2, title: "Dos de uno", releaseDate: "16-07-1997", season: seasonOne)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testEpisodeExistance() {
        XCTAssertNotNil(espisodeOneOfOne)
        XCTAssertNotNil(espisodeOneOfTwo)
    }
    
    func testEpisodeDescription() {
        let espisodeOneOfOneEqual = Episode(order: 1, title: "Uno de uno", releaseDate: "16-07-1997", season: seasonOne)
        XCTAssertEqual(espisodeOneOfOne.description, espisodeOneOfOneEqual.description)
    }
    
    func testEpisodeEquality() {
        XCTAssertEqual(espisodeOneOfOne, espisodeOneOfOne)
        XCTAssertNotEqual(espisodeOneOfOne, espisodeOneOfTwo)
        
        let espisodeOneOfOneEqual = Episode(order: 1, title: "Uno de uno", releaseDate: "16-07-1997", season: seasonOne)
        let espisodeTwoOfOne = Episode(order: 2, title: "Dos de uno", releaseDate: "16-07-1997", season: seasonOne)
        XCTAssertEqual(espisodeOneOfOne, espisodeOneOfOneEqual)
        XCTAssertNotEqual(espisodeOneOfOne, espisodeTwoOfOne)
    }
    
    func testEpisodeHashable() {
        XCTAssertNotNil(espisodeOneOfOne.hashValue)
    }
    
    func testEpisodeComparison(){
        XCTAssertLessThan(espisodeOneOfOne, espisodeOneOfTwo)
    }
}
