//
//  HouseTests.swift
//  WesterosTests
//
//  Created by Jorge Beltran Nuñez on 08/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import XCTest
@testable import Westeros

class HouseTests: XCTestCase {
    
    var starkSigil: Sigil!
    var lannisterSigil: Sigil!
    var targaryenSigil: Sigil!
    
    var starkHouse: House!
    var lannisterHouse: House!
    var targaryenHouse: House!
    
    var robb: Person!
    var arya: Person!
    var tyrion: Person!
    
    override func setUp() {
        super.setUp()
        
        starkSigil = Sigil(image: UIImage(), description: "Lobo Huargo")
        lannisterSigil = Sigil(image: UIImage(), description: "Leon rampante")
        targaryenSigil = Sigil(image: #imageLiteral(resourceName: "targaryenSmall.jpg"), description: "Dragon Tricefalo")
        
        starkHouse = House.getInstance(name: .stark, sigil: starkSigil, words: "Se acerca el invierno", url: URL(string: "http://awoiaf.westeros.org/index.php/House_Stark")!)
        lannisterHouse = House.getInstance(name: .lannister, sigil: lannisterSigil, words: "Oye mi rugido", url: URL(string: "http://awoiaf.westeros.org/index.php/House_Lannister")!)
        targaryenHouse = House.getInstance(name: .targaryen, sigil: targaryenSigil, words: "Fuego y sangre", url: URL(string: "https://awoiaf.westeros.org/index.php/House_Targaryen")!)
        
        robb = Person(name: "Robb", alias: "El joven lobo", house: starkHouse)
        arya = Person(name: "Arya", house: starkHouse)
        
        _ = Person(name: "Tyrion", alias: "El enano", house: lannisterHouse)
        _ = Person(name: "Cersei", house: lannisterHouse!)
        _ = Person(name: "Jaime", alias: "El Matarreyes", house: lannisterHouse!)
        
        _ = Person(name: "Daenerys", alias: "Madre de dragones", house: targaryenHouse!)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testHouseExistance() {
        XCTAssertNotNil(starkHouse)
        XCTAssertNotNil(lannisterHouse)
    }
    
    func testSigilExistence () {
        XCTAssertNotNil(starkSigil)
        XCTAssertNotNil(lannisterSigil)
    }
    
    func testAddPersons(){
        
        var countPerson = targaryenHouse.count
        _ = Person(name: "Daenerys", alias: "Madre de dragones", house: targaryenHouse)
        XCTAssertEqual(targaryenHouse.count, countPerson)
        
        countPerson = targaryenHouse.count
        _ = Person(name: "Aemon", house: targaryenHouse)
        XCTAssertEqual(targaryenHouse.count, countPerson + 1)
    }
    
    func testHouseEquality() {
        // Identidad
        XCTAssertEqual(starkHouse, starkHouse)
        
        // Igualdad por Multiton
//        let jinxed = HouseMultiton.getInstance(name: .stark, sigil: starkSigil, words: "Se acerca el invierno", url: URL(string: "http://awoiaf.westeros.org/index.php/House_Stark")!)
//        XCTAssertEqual(jinxed, starkHouse)
        
        // Desigualdad
        XCTAssertNotEqual(starkHouse, lannisterHouse)
    }
    
    func testHouseHashable() {
        XCTAssertNotNil(starkHouse.hashValue)
    }
    
    func testHouseComparison(){
        XCTAssertLessThan(lannisterHouse, starkHouse)
    }
    
    func testHouseReturnsSortedArrayOfMembers() {
        XCTAssertEqual(starkHouse.sortedMembers, [arya, robb])
    }
    
}

