//
//  House.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 08/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit


typealias Words = String
typealias Members = Set<Person>

enum HouseName: String {
    case lannister = "Lannister"
    case stark = "Stark"
    case targaryen = "Targaryen"
}

// MARK: - House
final class House {
    let name: HouseName
    let sigil: Sigil
    let words: Words
    let wikiURL: URL
    private var _members: Members
    
    fileprivate init(name: HouseName, sigil: Sigil, words: Words, url: URL) {
        self.name = name
        self.sigil = sigil
        self.words = words
        self.wikiURL = url
        _members = Members()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(addPersonOfNotification), name: Notification.Name("\(PERSON_HAS_BEEN_CREATED)_\(name)"), object: nil)
    }
}

// MARK: - Properties array members
extension House {
    var count: Int {
        return _members.count
    }
    
    var sortedMembers: [Person] {
        return _members.sorted()
    }
    
    func add (person: Person){
        guard person.house == self else {
            return
        }
        _members.insert(person)
    }
    
    func add(persons: Person...) {
        persons.forEach{add(person: $0)}
    }
}

// MARK: - Notifications
extension House {
    @objc func addPersonOfNotification(notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }
        
        let person = info[PERSON_KEY] as? Person
        
        add(person: person!)
    }
}

// MARK: - Proxy String
extension House {
    var proxyForEquiality: String {
        return "\(name) \(words) \(count)"
    }
    
    var proxyForComparison: String {
        return name.rawValue.uppercased()
    }
}

// MARK: - Equatable
extension House: Equatable {
    static func ==(lhs: House, rhs: House) -> Bool {
        return lhs.proxyForEquiality == rhs.proxyForEquiality
    }
}

// MARK: - Hashable
extension House: Hashable {
    var hashValue: Int {
        return proxyForEquiality.hashValue
    }
}

// MARK: - Comparable
extension House: Comparable {
    static func <(lhs: House, rhs: House) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }
}

// MARK: - Sigil
final class Sigil {
    let description: String
    let image: UIImage
    
    init(image: UIImage, description: String){
        self.image = image
        self.description = description
    }
}

// MARK: - Multiton Pattern
extension House {
    private static var houses: [HouseName: House]?
    
    class func getInstance(name: HouseName, sigil: Sigil?, words: Words?, url: URL?) -> House {
        if (houses == nil) {
            houses = [HouseName: House]()
        }
        if (houses![name] == nil) {
            houses![name] = House(name: name, sigil: sigil!, words: words!, url: url!)
        }
        return houses![name]!
    }
}
