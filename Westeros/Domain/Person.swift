//
//  Character.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 08/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

let PERSON_HAS_BEEN_CREATED = "PERSON_HAS_BEEN_CREATED"
let PERSON_KEY = "PERSON_KEY"

final class Person {
    let name: String
    let house: House
    private let _alias: String?
    
    var alias: String {
        return _alias ?? ""
    }
    
    init(name:String, alias: String? = nil, house: House){
        self.name = name
        _alias = alias
        self.house = house
        
        let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name("\(PERSON_HAS_BEEN_CREATED)_\(house.name)"), object: self, userInfo: [PERSON_KEY: self])
        notificationCenter.post(notification)
    }
}

extension Person {
    var fullName: String {
        return "\(name) \(house.name.rawValue)"
    }
}

// MARK: - Proxy string
extension Person {
    var proxyForEquality: String {
        return "\(name) \(alias) \(house.name)"
    }
    var proxyForComparison: String {
        return fullName.uppercased()
    }
}

// MARK: - Hashable
extension Person: Hashable {
    var hashValue: Int {
        return proxyForEquality.hashValue
    }
}

// MARK: - Equatable
extension Person: Equatable {
    static func ==(lhs: Person, rhs: Person) -> Bool {
        return lhs.proxyForEquality == rhs.proxyForEquality
    }
}

// MARK: - Comparable
extension Person: Comparable {
    static func <(lhs: Person, rhs: Person) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }
}

