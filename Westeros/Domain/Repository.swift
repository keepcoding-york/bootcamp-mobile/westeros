//
//  Repository.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 13/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

final class Repository {
    static let local = LocalFactory()
}

protocol HouseFactory {
    typealias FilterHouse = (House) -> Bool
    var houses: [House] { get }
    func house(named: String) -> House?
    func houses(filteredBy: FilterHouse) -> [House]
}

protocol SeasonFactory {
    typealias FilterSeason = (Season) -> Bool
    var seasons: [Season] { get }
    func season(titled: String) -> Season?
    func seasons(filteredBy: FilterSeason) -> [Season]
}

final class LocalFactory: HouseFactory {
    
    var houses: [House] {
        
        let starkSigil = Sigil(image: #imageLiteral(resourceName: "codeIsComing.png"), description: "Lobo Huargo")
        let lannisterSigil = Sigil(image: #imageLiteral(resourceName: "lannister.jpg"), description: "Leon rampante")
        let targaryenSigil = Sigil(image: #imageLiteral(resourceName: "targaryenSmall.jpg"), description: "Dragon Tricefalo")
        
        let starkHouse = House.getInstance(name: .stark, sigil: starkSigil, words: "Se acerca el invierno", url: URL(string: "https://awoiaf.westeros.org/index.php/House_Stark")!)
        let lannisterHouse = House.getInstance(name: .lannister, sigil: lannisterSigil, words: "Oye mi rugido", url: URL(string: "https://awoiaf.westeros.org/index.php/House_Lannister")!)
        let targaryenHouse = House.getInstance(name: .targaryen, sigil: targaryenSigil, words: "Fuego y sangre", url: URL(string: "https://awoiaf.westeros.org/index.php/House_Targaryen")!)
        
        _ = Person(name: "Robb", alias: "El joven lobo", house: starkHouse)
        _ = Person(name: "Arya", house: starkHouse)
        
        _ = Person(name: "Tyrion", alias: "El enano", house: lannisterHouse)
        _ = Person(name: "Cersei", house: lannisterHouse)
        _ = Person(name: "Jaime", alias: "El Matarreyes", house: lannisterHouse)
        
        _ = Person(name: "Daenerys", alias: "Madre de dragones", house: targaryenHouse)
        
        return [starkHouse, lannisterHouse, targaryenHouse].sorted()
    }
    
    // MARK: - Filters house
    func house(named name: String) -> House? {
        let house = houses.filter{$0.name.rawValue.uppercased() == name.uppercased()}.first
        return house
    }
    
    func house(named name: HouseName) -> House? {
        let house = houses.filter{$0.name == name}.first
        return house
    }
    
    func houses(filteredBy: FilterHouse) -> [House] {
        return houses.filter(filteredBy)
    }
}

extension LocalFactory: SeasonFactory {

    var seasons: [Season] {
        let seasonOne = Season(order: 1, title: "Temporada uno", releaseDate: "16-07-1997")
        let seasonTwo = Season(order: 2, title: "Temporada dos", releaseDate: "16-07-1998")
        let seasonThree = Season(order: 3, title: "Temporada tres", releaseDate: "16-07-1999")
        let seasonFour = Season(order: 4, title: "Temporada cuatro", releaseDate: "16-07-2000")
        let seasonFive = Season(order: 5, title: "Temporada cinco", releaseDate: "16-07-2001")
        let seasonSix = Season(order: 6, title: "Temporada seis", releaseDate: "16-07-2002")
        let seasonSeven = Season(order: 7, title: "Temporada siete", releaseDate: "16-07-2003")
        
        _ = Episode(order: 1, title: "Uno de uno", releaseDate: "16-07-1997", season: seasonOne)
        _ = Episode(order: 2, title: "Dos de uno", releaseDate: "16-07-1997", season: seasonOne)
        
        _ = Episode(order: 1, title: "Uno de dos", releaseDate: "16-07-1997", season: seasonTwo)
        _ = Episode(order: 2, title: "Dos de dos", releaseDate: "16-07-1997", season: seasonTwo)
        
        _ = Episode(order: 1, title: "Uno de tres", releaseDate: "16-07-1997", season: seasonThree)
        _ = Episode(order: 2, title: "Dos de tres", releaseDate: "16-07-1997", season: seasonThree)
        
        _ = Episode(order: 1, title: "Uno de cuatro", releaseDate: "16-07-1997", season: seasonFour)
        _ = Episode(order: 2, title: "Dos de cuatro", releaseDate: "16-07-1997", season: seasonFour)
        
        _ = Episode(order: 1, title: "Uno de cinco", releaseDate: "16-07-1997", season: seasonFive)
        _ = Episode(order: 2, title: "Dos de cinco", releaseDate: "16-07-1997", season: seasonFive)
        
        _ = Episode(order: 1, title: "Uno de seis", releaseDate: "16-07-1997", season: seasonSix)
        _ = Episode(order: 2, title: "Dos de seis", releaseDate: "16-07-1997", season: seasonSix)
        
        _ = Episode(order: 1, title: "Uno de siete", releaseDate: "16-07-1997", season: seasonSeven)
        _ = Episode(order: 2, title: "Dos de siete", releaseDate: "16-07-1997", season: seasonSeven)
        
        return [seasonOne, seasonTwo, seasonThree, seasonFour, seasonFive, seasonSix, seasonSeven].sorted()
    }
    
    // MARK: - Filters season
    func season(titled title: String) -> Season? {
        let season = seasons.filter{$0.title.uppercased() == title.uppercased()}.first
        return season
    }
    
    func seasons(filteredBy: FilterSeason) -> [Season] {
        return seasons.filter(filteredBy)
    }
}
