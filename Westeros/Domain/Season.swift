//
//  Season.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 22/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

typealias Episodes = Set<Episode>

class Season {
    let order: Int
    let title: String
    let releaseDate: Date
    var episodes: Episodes
    
    init(order: Int, title: String, releaseDate: String) {
        self.order = order
        self.title = title
        self.releaseDate = releaseDate.toDate(dateFormatType: .date)!
        self.episodes = Episodes()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(addEpisodeOfNotification), name: Notification.Name("\(EPISODE_HAS_BEEN_CREATED)_\(proxyForComparison)"), object: nil)
    }
}

// MARK: - Properties array episodes
extension Season {
    var count: Int {
        return episodes.count
    }
    
    var sortedEpisodes: [Episode] {
        return episodes.sorted()
    }
    
    func add (episode: Episode){
        guard episode.season == self else {
            return
        }
        episodes.insert(episode)
    }
    
    func add(episodes: Episode...) {
        episodes.forEach{add(episode: $0)}
    }
}

// MARK: - Notifications
extension Season {
    @objc func addEpisodeOfNotification(notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }
        
        let episode = info[EPISODE_KEY] as? Episode
        
        add(episode: episode!)
    }
}

// MARK: - Proxy string
extension Season {
    var proxyForEquiality: String {
        return "\(order) \(title) \(releaseDate)"
    }
    
    var proxyForComparison: String {
        return proxyForEquiality.uppercased()
    }
}

// MARK: - CustomStringConvertible
extension Season: CustomStringConvertible {
    var description: String {
        return "Temporada: \(order) Título: \(title) Fecha de lanzamiento: \(releaseDate)"
    }
}

// MARK: - Hashable
extension Season: Hashable {
    var hashValue: Int {
        return proxyForEquiality.hashValue
    }
}

// MARK: - Comparable
extension Season: Comparable {
    static func <(lhs: Season, rhs: Season) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }
}

// MARK: - Equatable
extension Season: Equatable {
    static func ==(lhs: Season, rhs: Season) -> Bool {
        return lhs.proxyForEquiality == rhs.proxyForEquiality
    }
}
