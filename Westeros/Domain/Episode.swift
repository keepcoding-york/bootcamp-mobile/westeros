//
//  Episode.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 22/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation

let EPISODE_HAS_BEEN_CREATED = "EPISODE_HAS_BEEN_CREATED"
let EPISODE_KEY = "EPISODE_KEY"

class Episode {
    let order: Int
    let title: String
    let releaseDate: Date
    weak var season: Season?
    
    init(order: Int, title: String, releaseDate: String, season: Season) {
        self.order = order
        self.title = title
        self.releaseDate = releaseDate.toDate(dateFormatType: .date)!
        self.season = season
        
        let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name("\(EPISODE_HAS_BEEN_CREATED)_\(season.proxyForComparison)"), object: self, userInfo: [EPISODE_KEY: self])
        notificationCenter.post(notification)
    }
}

// MARK: - Proxy string
extension Episode {
    var proxyForEquiality: String {
        return "\(order) \(title) \(releaseDate) \(season!.title)"
    }
    
    var proxyForComparison: String {
        return proxyForEquiality.uppercased()
    }
}

// MARK: - CustomStringConvertible
extension Episode: CustomStringConvertible {
    var description: String {
        return "Episodio: \(order) Título: \(title) Fecha de lanzamiento: \(releaseDate) Temporada: \(season!.title)"
    }
}

// MARK: - Hashable
extension Episode: Hashable {
    var hashValue: Int {
        return proxyForEquiality.hashValue
    }
}

// MARK: - Comparable
extension Episode: Comparable {
    static func <(lhs: Episode, rhs: Episode) -> Bool {
        return lhs.proxyForComparison < rhs.proxyForComparison
    }
}

// MARK: - Equatable
extension Episode: Equatable {
    static func ==(lhs: Episode, rhs: Episode) -> Bool {
        return lhs.proxyForEquiality == rhs.proxyForEquiality
    }
}

