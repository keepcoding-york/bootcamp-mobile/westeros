//
//  MemberDetailViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 27/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class MemberDetailViewController: UIViewController {
    
    // Mark: - Properties
    private var model: Person
    var label: UILabel!
    
    // Mark: - Initialization
    init(model: Person) {
        self.model =  model
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        setupUI()
        setupAutoLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Mark: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        syncModelWithView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(houseDidChange), name: Notification.Name(HOUSE_DID_CHANGE_NOTIFICATION_CHANGE), object: nil)
    }
    
    // MARK: - Notifications
    @objc func houseDidChange(notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }
        
        let house = info[HOUSE_KEY] as? House
        
        model =  house!.sortedMembers.first!
        if navigationController?.topViewController == self {
            navigationController?.popViewController(animated: true)
        }
        syncModelWithView()
    }
    
    // MARK: - Sync
    func syncModelWithView(){
        label.text = model.fullName
    }
    
    func setupUI() {
        label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.backgroundColor = .gray
        label.textColor = .white
        self.view.addSubview(label)
        
        self.view.backgroundColor = .black
    }
    
    func setupAutoLayout() {
        
        // Pin left edge
        let margins = self.view.layoutMarginsGuide
        label.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20).isActive = true
        label.topAnchor.constraint(equalTo: margins.topAnchor, constant: 20).isActive = true
        
        // Center in container
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
}

