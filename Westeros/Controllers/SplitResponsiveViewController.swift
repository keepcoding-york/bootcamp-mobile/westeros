//
//  SplitResponsiveViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 27/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class SplitResponsiveViewController: UISplitViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SplitResponsiveViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}
