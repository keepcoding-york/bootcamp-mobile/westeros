//
//  EpisodeDetailViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 24/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class EpisodeDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var seasonLabel: UILabel!
    
    // MARK: - Properties
    private var model: Episode
    
    // MARK: - Initialization
    init(model: Episode){
        self.model = model;
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        syncModelWithView()
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self, selector: #selector(seasonDidChange), name: Notification.Name(SEASON_DID_CHANGE_NOTIFICATION_CHANGE), object: nil)
    }
    
    // MARK: - Notifications
    @objc func seasonDidChange(notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }
        
        let season = info[SEASON_KEY] as? Season
        
        model =  season!.sortedEpisodes.first!
        
        if navigationController?.topViewController == self {
            navigationController?.popViewController(animated: true)
        }
        
        syncModelWithView()
    }
    
    // MARK: - Sync
    func syncModelWithView(){
        titleLabel.text = "\(model.title)"
        orderLabel.text = "Number of Episodes: \(model.order)"
        releaseDateLabel.text = "Release Date: \(model.releaseDate.toString(dateFormatType: .date)!)"
        seasonLabel.text = "Season: \(model.season!.title)"
        
        title = model.title
    }
}
