//
//  HouseListViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 15/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

let HOUSE_DID_CHANGE_NOTIFICATION_CHANGE = "HouseDidChange"
let HOUSE_KEY = "HouseKey"

protocol HouseListViewControllerDelegate: class {
    func houseListViewController(_ vc: HouseListViewController, didSelectHouse: House)
}

class HouseListViewController: UITableViewController {
    
    // MARK: - Properties
    let model: [House]
    weak var delegate: HouseListViewControllerDelegate?
    var houseDetailViewController: UIViewController?
    
    // MARK: - Initialization
    init(model: [House]){
        self.model = model
        super.init(style: .plain)
        title = "Westeros"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let lastRow = UserDefaults.standard.integer(forKey: LAST_SELECTED)
        let indexPath = IndexPath(row: lastRow, section: 0)
        
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "HouseCell"
        let house = model[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) ?? UITableViewCell(style: .default, reuseIdentifier: cellId)
        
        cell.imageView?.image = house.sigil.image
        cell.textLabel?.text = house.name.rawValue
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let house = model[indexPath.row]
        
        if (splitViewController!.isCollapsed) {
            splitViewController?.showDetailViewController((houseDetailViewController?.wrappedInNavigation())!, sender: nil)
        }
        
        delegate?.houseListViewController(self, didSelectHouse: house)
        
        let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name(HOUSE_DID_CHANGE_NOTIFICATION_CHANGE), object: self, userInfo: [HOUSE_KEY: house])
        
        notificationCenter.post(notification)
        
        saveLastSelected(at: indexPath.row)
    }
}

extension HouseListViewController: SaveSelected  {
    var LAST_SELECTED: String {
        return "LAST_HOUSE"
    }
}


