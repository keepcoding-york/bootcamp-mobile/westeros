//
//  HouseDetailViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 12/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class HouseDetailViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var houseNameLabel: UILabel!
    @IBOutlet weak var sigilImageView: UIImageView!
    @IBOutlet weak var wordsLabel: UILabel!
    
    // MARK: - Properties
    var model: House
    var isSubviewsCreated: Bool = false
    
    // MARK: - Initialization
    init(model: House){
        self.model = model;
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        title = model.name.rawValue
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isSubviewsCreated = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        syncModelWithView()
    }
    
    // MARK: - Sync
    func syncModelWithView(){
        houseNameLabel.text = "House \(model.name)"
        sigilImageView.image = model.sigil.image
        wordsLabel.text = model.words
        
        title = model.name.rawValue
        
        let backItem = UIBarButtonItem()
        backItem.title = model.name.rawValue
        navigationItem.backBarButtonItem = backItem
    }
    
    // MARK: - UI
    func setupUI() {
        let members = UIBarButtonItem(title: "Members", style: UIBarButtonItemStyle.plain, target: self, action: #selector(displayMembers))
        let wikiButton = UIBarButtonItem(title: "Wiki", style: UIBarButtonItemStyle.plain, target: self, action: #selector(displayWiki))
        
        navigationItem.rightBarButtonItems = [wikiButton, members]
    }
    
    // MARK: - Action for buttons
    @objc func displayMembers(){
        let memberListViewController = MemberListViewController(model: model.sortedMembers)
        navigationController?.pushViewController(memberListViewController, animated: true)
    }
    
    @objc func displayWiki(){
        let wikiViewController = WikiViewController(model: model)
        navigationController?.pushViewController(wikiViewController, animated: true)
    }
}

extension HouseDetailViewController: HouseListViewControllerDelegate {
    func houseListViewController(_ vc: HouseListViewController, didSelectHouse house: House) {
        self.model = house
        if isSubviewsCreated {
            syncModelWithView()
        }
    }
}
