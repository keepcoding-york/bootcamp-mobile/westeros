//
//  SeasonListViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 23/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

let SEASON_DID_CHANGE_NOTIFICATION_CHANGE = "SeasonDidChange"
let SEASON_KEY = "SeasonKey"

protocol SeasonListViewControllerDelegate: class {
    func seasonListViewController(_ vc: SeasonListViewController, didSelectSeason: Season)
}

class SeasonListViewController: UITableViewController {
    
    // MARK: - Properties
    let model: [Season]
    weak var delegate: SeasonListViewControllerDelegate?
    var seasonDetailViewController: UIViewController?
    
    // MARK: - Initialization
    init(model: [Season]){
        self.model = model
        super.init(style: .plain)
        title = "Westeros Season"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let lastRow = UserDefaults.standard.integer(forKey: LAST_SELECTED)
        let indexPath = IndexPath(row: lastRow, section: 0)
        
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "SeasonCell"
        let season = model[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) ?? UITableViewCell(style: .default, reuseIdentifier: cellId)
        
        cell.textLabel?.text = season.title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let season = model[indexPath.row]
        
        if (splitViewController!.isCollapsed) {
            splitViewController?.showDetailViewController((seasonDetailViewController?.wrappedInNavigation())!, sender: nil)
        }
        
        delegate?.seasonListViewController(self, didSelectSeason: season)
        
        let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name(SEASON_DID_CHANGE_NOTIFICATION_CHANGE), object: self, userInfo: [SEASON_KEY: season])
        
        notificationCenter.post(notification)
        
        saveLastSelected (at: indexPath.row)
    }
}

extension SeasonListViewController: SaveSelected  {
    var LAST_SELECTED: String {
       return "LAST_SEASON"
    }
}

