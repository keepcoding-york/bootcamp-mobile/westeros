//
//  SeasonDetailViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 23/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

class SeasonDetailViewController: UIViewController {
    
    @IBOutlet weak var seasonTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var numberEpisodesLabel: UILabel!
    
    // MARK: - Properties
    var model: Season
    var isSubviewsCreated: Bool = false
    
    // MARK: - Initialization
    init(model: Season){
        self.model = model;
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        title = model.title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        isSubviewsCreated = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        syncModelWithView()
    }
    
    // MARK: - Sync
    func syncModelWithView(){
        seasonTitleLabel.text = "\(model.title)"
        releaseDateLabel.text = "Release Date: \(model.releaseDate.toString(dateFormatType: .date)!)"
        numberEpisodesLabel.text = "Number of Episodes: \(model.count)"
        title = model.title
        
        let backItem = UIBarButtonItem()
        backItem.title = model.title
        navigationItem.backBarButtonItem = backItem
    }
    
    // MARK: - UI
    func setupUI() {
        let espisodes = UIBarButtonItem(title: "Episodes", style: UIBarButtonItemStyle.plain, target: self, action: #selector(displayListOfEpisodes))
        navigationItem.rightBarButtonItems = [espisodes]
    }
    
    // MARK: - Action for buttons
    @objc func displayListOfEpisodes(){
        let episodeListViewController = EpisodeListViewController(model: model.sortedEpisodes)
        navigationController?.pushViewController(episodeListViewController, animated: true)
    }
}

extension SeasonDetailViewController: SeasonListViewControllerDelegate {
    func seasonListViewController(_ vc: SeasonListViewController, didSelectSeason season: Season) {
        self.model = season
        if isSubviewsCreated {
            syncModelWithView()
        }
    }
}
