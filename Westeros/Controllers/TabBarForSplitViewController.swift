//
//  TabBarForSplitViewController.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 26/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

typealias masterAndDetail = (master: UIViewController, detail: UIViewController)

class TabBarForSplitViewController: UITabBarController {
    
    let masterViews: [UIViewController]
    let detailViews: [UIViewController]
    let detailViewController: UINavigationController
    var currentState: Int = 0
    
    init(listOfMasterAndDetail: [masterAndDetail], detailViewController: UINavigationController) {
        masterViews = listOfMasterAndDetail.map({$0.master})
        detailViews = listOfMasterAndDetail.map({$0.detail})
        self.detailViewController = detailViewController
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        viewControllers = masterViews
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isCurrentState(_ state: Int) -> Bool {
        if currentState == state {
            return false
        } else {
            currentState = state
            return true
        }
    }
}

extension TabBarForSplitViewController: UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        // TODO: FIX ME AND CLEAN UP
//        if isCurrentState(selectedIndex) {
// //            showDetailViewController(detailViews[selectedIndex].wrappedInNavigation(), sender: nil)
//            detailViewController.setViewControllers([detailViews[selectedIndex]], animated: true)
//        }
        
        if (isCurrentState(selectedIndex) && !splitViewController!.isCollapsed) {
            showDetailViewController(detailViews[selectedIndex].wrappedInNavigation(), sender: nil)
//            detailViewController.setViewControllers([detailViews[selectedIndex]], animated: true)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if selectedViewController == nil || viewController == selectedViewController {
            return false
        }
        
        let fromView = selectedViewController!.view
        let toView = viewController.view
        
        UIView.transition(from: fromView!, to: toView!, duration: 0.15, options: [.transitionCrossDissolve], completion: nil)
        
        return true
    }
}

