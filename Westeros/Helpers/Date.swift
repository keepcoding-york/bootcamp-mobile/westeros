//
//  String.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 22/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import Foundation


public extension String {
    func toDate(dateFormatType: DateFormatType) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatType.rawValue
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let date: Date? = dateFormatter.date(from: self)
        return date
    }
}

public extension Date {
    func toString(dateFormatType: DateFormatType) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatType.rawValue
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        let dateString: String? = dateFormatter.string(from: self)
        return dateString
    }
}

public enum DateFormatType: String {
    // Time
    case time = "HH:mm:ss"
    
    // Date with hours
    case dateWithTime = "yyyy-MM-dd HH:mm:ss"
    
    // Date
    case date = "dd-MM-yyyy"
}
