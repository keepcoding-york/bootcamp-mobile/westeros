//
//  AppDelegate.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 08/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

//typealias masterAndDetail = (master: UITableViewController, detail: UIViewController)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.backgroundColor = .black
        window?.makeKeyAndVisible()
        
        // MARK: - Models
        let houses = Repository.local.houses
        let seasons = Repository.local.seasons
        
        // MARK: - Controllers
        var listOfMasterAndDetail: [masterAndDetail] = []
        
        let seasonListViewController = SeasonListViewController(model: seasons)
        let lastSelectedSeason = seasonListViewController.lastSelected()
        let seasonDetailViewController = SeasonDetailViewController(model: lastSelectedSeason)
        seasonListViewController.delegate = seasonDetailViewController
        seasonListViewController.seasonDetailViewController = seasonDetailViewController
        
        listOfMasterAndDetail.append((master: seasonListViewController, detail: seasonDetailViewController))
        
        let houseListViewController = HouseListViewController(model: houses)
        let lastSelectedHouse = houseListViewController.lastSelected()
        let houseDetailViewController = HouseDetailViewController(model: lastSelectedHouse)
        houseListViewController.delegate = houseDetailViewController
        houseListViewController.houseDetailViewController = houseDetailViewController
        
        listOfMasterAndDetail.append((master: houseListViewController, detail: houseDetailViewController))
        
        let splitViewController = SplitResponsiveViewController()

        let detailUIView =  UINavigationController(rootViewController: seasonDetailViewController)
        let tabBarForSplitView = TabBarForSplitViewController(listOfMasterAndDetail: listOfMasterAndDetail, detailViewController: detailUIView)
        
        splitViewController.viewControllers = [tabBarForSplitView.wrappedInNavigation(), detailUIView]
        
        // Sets rootVC
        window?.rootViewController = splitViewController
        
        return true
    }
}

