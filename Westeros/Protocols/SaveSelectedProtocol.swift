//
//  SaveSelectedProtocol.swift
//  Westeros
//
//  Created by Jorge Beltran Nuñez on 26/02/2018.
//  Copyright © 2018 Jorge Beltran Nuñez. All rights reserved.
//

import UIKit

protocol SaveSelected {
    associatedtype typeOfModel
    var LAST_SELECTED: String {get}
    var model: [typeOfModel] {get}
}

extension SaveSelected {

    func saveLastSelected (at row: Int) {
        let defaults = UserDefaults.standard
        defaults.set(row, forKey: LAST_SELECTED)
        defaults.synchronize()
    }
    
    func lastSelected () -> typeOfModel {
        let defaults = UserDefaults.standard
        let row = defaults.integer(forKey: LAST_SELECTED)
    
        let selected = model[row]
    
        return selected
    }
}
