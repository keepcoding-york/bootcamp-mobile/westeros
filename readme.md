# App WESTEROS for iOS

Aplicación para mostrar datos relacionados con Juego de tronos, como las temporadas e información de la serie.

Tabla de contenidos:
- [App WESTEROS for iOS](#app-westeros-for-ios)
    - [¿Cómo podríamos hacer para que al crear un nuevo personaje y asignarle la casa, se actualice su propiedad _members 🤔 ?](#%C2%BFco%CC%81mo-podri%CC%81amos-hacer-para-que-al-crear-un-nuevo-personaje-y-asignarle-la-casa-se-actualice-su-propiedad-members-%F0%9F%A4%94)
    - [Instanciando House siguiendo el patrón Multiton](#instanciando-house-siguiendo-el-patr%C3%B3n-multiton)
    - [🚧  Mapa de ruta 🚧](#%F0%9F%9A%A7-mapa-de-ruta-%F0%9F%9A%A7)
        - [TabBarForSplitViewController.swift](#tabbarforsplitviewcontrollerswift)

## ¿Cómo podríamos hacer para que al crear un nuevo personaje y asignarle la casa, se actualice su propiedad _members 🤔 ?

Siguiendo el patrón pubSub he desacoplado la clases **House** y **Person** para que evitar que en el tiempo de crear una persona sea necesario el uso de un house previamente creado.

Para ello he creado un evento al cual se suscribe cada casa cuando es creada, se compone del tópico `PERSON_HAS_BEEN_CREATED` y el subtópico `name` del house:
``` swift
let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(addPersonOfNotification), name: Notification.Name("\(PERSON_HAS_BEEN_CREATED)_\(name)"), object: nil)
```

En person se emite el evento con su correspondiente tópico y subtópico de esta forma:

``` swift
let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name("\(PERSON_HAS_BEEN_CREATED)_\(house.name)"), object: self, userInfo: [PERSON_KEY: self])
        notificationCenter.post(notification)
```

De esta forma en un futuro se podría crear personas desde cualquier lado de la aplicación sin necesidad de tener una instancia a la casa que pertenece.

## Instanciando House siguiendo el patrón Multiton

Este patrón da consistencia al patrón pubSub seguido para agregar personas como miembros de casa. De esta forma te aseguras que nunca se pueda crear varias instancias de una misma clase:

``` swift
// MARK: - Multiton Pattern
extension House {
    private static var houses: [HouseName: House]?
    
    class func getInstance(name: HouseName, sigil: Sigil?, words: Words?, url: URL?) -> House {
        if (houses == nil) {
            houses = [HouseName: House]()
        }
        if (houses![name] == nil) {
            houses![name] = House(name: name, sigil: sigil!, words: words!, url: url!)
        }
        return houses![name]!
    }
}
```

## 🚧  Mapa de ruta 🚧 

* [ ] Usar JSON para rellenar modelos (Se dará en el módulo iOS avanzado)
* [ ] Dar el un mejor enfoque al controlador **tabBar** y como mostrar el **detail** del **splitview** tanto si esta colapsado o no:

### TabBarForSplitViewController.swift

```swift
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        // TODO: FIX ME AND CLEAN UP
//        if isCurrentState(selectedIndex) {
// //            showDetailViewController(detailViews[selectedIndex].wrappedInNavigation(), sender: nil)
//            detailViewController.setViewControllers([detailViews[selectedIndex]], animated: true)
//        }
        
        if (isCurrentState(selectedIndex) && !splitViewController!.isCollapsed) {
            showDetailViewController(detailViews[selectedIndex].wrappedInNavigation(), sender: nil)
//            detailViewController.setViewControllers([detailViews[selectedIndex]], animated: true)
        }
    }
```